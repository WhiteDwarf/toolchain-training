package nl.praegus;

import fitnesse.junit.FitNesseRunner;
import nl.praegus.fitnesse.junit.runner.ToolchainRunnerWithStandaloneListener;
import nl.praegus.fitnesse.junit.runner.ToolchainTestRunner;
import nl.praegus.fitnesse.junit.runners.ReportPortalEnabledToolchainTestRunner;
import org.junit.runner.RunWith;

/**
 * Test class to allow fixture code to be debugged, or run by build server.
 */
@RunWith(ToolchainRunnerWithStandaloneListener.class)
@FitNesseRunner.Suite("ToolchainDocs.SikuliTest")
public class FixtureDebugTest {
}
