package nl.praegus.fitnesse.junit.runner;

import fitnesse.junit.DescriptionFactory;
import fitnesse.testrunner.MultipleTestsRunner;
import fitnesse.wiki.WikiPage;
import nl.hsac.fitnesse.junit.HsacFitNesseRunner;
import nl.praegus.fitnesse.StandaloneHtmlListener;
import nl.praegus.fitnesse.junit.testsystemlisteners.ConsoleLogListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

import java.util.List;

public class ToolchainRunnerWithStandaloneListener extends HsacFitNesseRunner {
    public ToolchainRunnerWithStandaloneListener(Class<?> suiteClass) throws InitializationError {
        super(suiteClass);
        System.getProperties().setProperty("nodebug", "true");
    }

    protected void runPages(List<WikiPage> pages, RunNotifier notifier) {
        super.runPages(pages, notifier);
    }

    protected void addTestSystemListeners(RunNotifier notifier, MultipleTestsRunner testRunner, Class<?> suiteClass, DescriptionFactory descriptionFactory) {
        super.addTestSystemListeners(notifier, testRunner, suiteClass, descriptionFactory);
        testRunner.addTestSystemListener(new StandaloneHtmlListener());
        testRunner.addTestSystemListener(new ConsoleLogListener());
    }
}
