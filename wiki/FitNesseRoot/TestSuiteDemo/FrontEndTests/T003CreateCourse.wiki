---
Suites: plainMethods
Test
---
|script                                       |
|click        |css=.mx-demouserswitcher-toggle|
|click        |demo_administrator             |
|wait for page|Praegus - Dashboard            |

|script                                                                                              |
|click                |Administration                                                                |
|click                |Course                                                                        |
|click                |New                                                                           |
|set search context to|css=.modal-content                                                            |
|enter                |Java for Testers|as         |Name                                             |
|enter                |It's great!     |as         |Description                                      |
|select               |Academy         |for        |Category                                         |
|click                |Save                                                                          |
|clear search context                                                                                |
|wait for visible     |Java for Testers                                                              |
|check                |value of        |Description|in row where|Name|is|Java for Testers|It's great!|