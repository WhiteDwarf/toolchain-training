---
Help: My first test suite (this is the help text, edit by clicking Edit)
Suites: this is a tag, you can add, and remove, by clicking edit
---
!2 Welcome home
!3 First thing's first
For a flying start, a few basics for FitNesse and the fixtures behind it need to be set.

!style_tooltipcustom[Text!style_tooltiptext tooltipcustombottom(Helloo, I am your friendly neighborhood tooltip)]

!4 1 Fixture location
As the first step, we will make sure FitNesse can find it's fixtures. The fixtures contain all the working code to talk to API's, files, Selenium and so on. Without setting that, FitNesse hasn't got a clue where to find the functions you're trying to execute. You can extend this later, if you start adding your own fixtures, or even set it specifically for a bunch of Test Pages or Suites.
The classpath location when working standalone:

!path fixtures
!path fixtures/*.jar

!4 2 Setting the Test System to use
Now that's out of the way, we have a 2nd thing to do.

As the second step, we set the test system. FitNesse has two ways of talking to your SUT (System Under Test), one is via the Fit route (explained in detail in the .FitNesse.UserGuide). The other one is via the SLIM route. SLIM is what most of the HSAC fixtures use. To configure this properly for all underlying pages, we will define it here. Details about this are also in the .FitNesse.UserGuide.

!define TEST_SYSTEM {slim}
And of course, to view the contents of this suite, we will add the contents, if you click Edit (or press E on your keyboard), you'll see a line like{{{!contents -R2 -g -p -f -h}}}For more details about the configuration of your Table of Contents, consult the .FitNesse.UserGuide.

Let's move on to the test suites that are already here. Select a page from the table of contents below.

!contents -R2 -g -p -f -h

!define COLLAPSE_SETUP {true}
!define COLLAPSE_TEARDOWN {true}